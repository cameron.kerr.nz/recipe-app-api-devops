// BELIEF: this is the DNS zone we will be operating in. No issues there.
//
data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

// BELIEF: this creates a CNAME record that points to our AWS ELB instance for our application.
// This is where it departs a little from the documentation, but I'm not sure if the documentation
// is just being a bit sparse, or if the validation records are meant to also be part of this object.
//
resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  type    = "CNAME"
  ttl     = "300"

  records = [aws_lb.api.dns_name]
}

// BELIEF: this models the certificate for our application.
//
resource "aws_acm_certificate" "app_cert" {

  // note that aws_route53_record.app.fqdn is known after apply,
  // but we have specified .name instead of .fqdn
  // in the aws_route53_record.app
  //
  domain_name = aws_route53_record.app.name

  validation_method = "DNS"

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true
  }
}
