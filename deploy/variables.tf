variable "prefix" {
  default = "raad"
}

variable "s3_prefix" {
  description = "Prefix to add to S3 bucket names to make them globally unique"
  default     = "cknz-"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "cameron.kerr.nz@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  description = "Name of AWS EC2 SSH Key-pair to use for bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "445804716994.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app-api-devops:main"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "445804716994.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app-api-revproxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "pungpungpanda.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
