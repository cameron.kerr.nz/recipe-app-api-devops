FROM python:3.7-alpine
LABEL maintainer="Cameron Kerr <cameron.kerr.nz@gmail.com>"

ENV PYTHONUNBUFFERED 1

# Alpine repository (dl-cdn) is hanging when run in GitLab.
# Use another mirror.
#
RUN sed -i -e 's/dl-cdn/dl-4/' /etc/apk/repositories

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev

RUN python -m venv /venv
RUN set -eu -o pipefail; \
    . /venv/bin/activate; \
    pip install --upgrade pip; \
    pip install -r /requirements.txt

RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app

ENV PATH="/scripts:${PATH}"
COPY ./scripts /scripts

RUN chmod 0755 /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web

USER user

VOLUME [ "/vol/web" ]
CMD [ "/scripts/entrypoint.sh" ]
